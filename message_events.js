const prefix= require("./prefix.json");
const { handle_commands,Command } = require("./commands");



var log=[];
var actions=[];

var handl={};

function get_handler(Y){
		if(!Y)
			return handl;
		handl.R=Y[1];
		handl.M=Y[0];
}



class message_act {
	constructor(Name,fun,extras) {
		this.Name=Name;
		this.fun=fun;
		this.extras=extras; //extras type list. Or json, with permission list, or role list.
		actions.push(this);
	}
	run(event){
		this.fun(event);
	}

}





class m_event {
	constructor(body,room_id,place,author,event_id) {

		this.event_id=event_id;
		this.place=place; //M or R , stand for Matrix or Revolt
		
		
		
		this.body=body;		
		this.room_id=room_id;
		this.author=author; //author ID
		log.push(this);
	}
	
	
	async get_username(author){//I might change this function
		if(this.place=='M')
			return author;
		let aut=author;
		if(!aut)
			aut=this.author;

		let event= await handl[this.place].get_event(this.event_id)
		
		
		let username = event.author.username
		return username;		
		
	}
	
	async get_pfp(author){//I might change this function
		let aut=author;
		if(!aut)
			aut=this.author;
		let pfp= await handl[this.place].get_pfp(author);
		if(!pfp)
			return null;
		return pfp;
	}
	
	async reply(content){
		
		let event= await handl[this.place].get_event(this.event_id,this.room_id);
		
		handl[this.place].reply(event,content);
		
	}
	reply_with_image(path){
	
		let h=handl[this.place];	
		
		if(typeof(path)!="string")
			return;
		if(path.includes("https"))
			h.send_image_url(null,path,this.room_id);
		else	
			h.send_image_path(null,path,this.room_id);
	}
	
	has_perm(permission){
		
		let event=handl[this.place].get_event(this.event_id,this.room_id);
		return handl[this.place].has_perm(event,permission);
		
	}
	redact(){
		
		let event=handl[this.place].get_event(this.event_id,this.room_id);
		handl[this.place].delete_msg(event);
		//log.splice();
	}
	react(reaction){
		
		
		return handl[this.place].react(this.event_id,reaction);
	}
	
}



async function handle_messages(event, room_id, place){
	if(handl[place]==undefined){
		console.log("ERROR, UNDEFINED HANDLER!!! AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA");
		return -1;
		}
	let id = handl[place].event_id(event);
	
	
	
	
	
	let M_event = new m_event(handl[place].get_body(event),room_id,place,handl[place].author_id(event),handl[place].event_id(event));
	
	//I dont remember why I placed this, It should work without it....
	if(place=="M")
		event.roomId=room_id;
	//but im not risking removing it
	
	
	
	
	M_event.reply_ids=handl[place].replies(event);
	M_event.body=handl[place].get_body(event);
	
	
	handle_commands(M_event,prefix.prefix,handl[place]);

	let i=0;
	for (i in actions){
		actions[i].run(M_event);
		
	}
	
	
	
	//let A=new m_event(handler[place].get_body(event),room_id,place,author,event_id);
	
	//console.log(A);
}











module.exports = { handle_messages,m_event, get_handler,message_act };