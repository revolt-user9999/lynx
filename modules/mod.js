
const { Command } = require("../commands");


function _kick(data){
	if(!data.args[1]){
		data.handler.reply(data.event,"Wrong usage, do "+data.args[0]+" UserID");
	}
	
	data.handler.kick(data.event,data.args[1]).then(res=>{
		
	
	if(res<0)
		data.handler.reply(data.event,"Error, the bot doesnt have permission to kick");
		
	})
}

function _ban(data){
	if(!data.args[1]){
		data.handler.reply(data.event,"Wrong usage, do "+data.args[0]+" UserID");
	}
	
	data.handler.ban(data.event,data.args[1]).then(res=>{
		
	
	if(res<0)
		data.handler.reply(data.event,"Error, the bot doesnt have permission to ban");
		
	})
}




function _echo(data){
	data.event.reply(data.args[1]);
	
}


const echo = new Command("echo",(data) => {return _echo(data);},null); //null means it requires no permission



const kick = new Command("kick",(data) => {return _kick(data);},{permissions: ["KickMembers"],});


const ban = new Command("ban",(data) => {return _ban(data);},{permissions: ["BanMembers"],});


