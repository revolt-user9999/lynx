## Lynx


Lynx is supposed to be a general purpose bot, while also having Image generation capabilities.
it can generate images using stable diffusion with the  COMFYUI webui. 
Proudly hosted by crispy.cat 
##### psst check out his cool site

this is a self-fork? I could make a second branch, but like it would get confusing. but Im using different tokens for the image generation and the bridge, to not get one rate-limited.





## Revolt-matrix- bot/bridge

This is a idea of a bot that could work and support with both matrix and revolt, With easily adding features without worrying about specifities of each API wrapper. 

Current features:
Bridge, kick, ban. echo.






## Usage
Edit config.json, edit the database URL in [line 3](https://gitlab.com/revolt-bot/Revolt-matrix-template/-/blob/main/utils/database.js) install the dependencies then >node index

## Support
Issues or idk DM @plug4090:4d2.org on matrix

## Roadmap
Planned features:
Counting, edit events on the bridge, wikipedia, fun images, memes, mute users.
Making the template easier to understand :Perhaps


## Contributing

Im open for contributions. Ill read the PRs

## Authors and acknowledgment
ShadowLP for telling me how to use his revolt-uploader library.


## License
MIT license

## Project status
Im running out of time. I might add a few more features, but the bot template will not change much
