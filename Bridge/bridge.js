const { message_act }=require("../message_events");

const { rand_string,object_updates } = require("../utils/utils");
const { db } = require("../utils/database");
const { Command } = require("../commands");
const config = require("../config.json");

const {send_masq_image,send_masq_message,parse_channel} = require("./send_events");

var dbb=new db();



	class channel{
	constructor(Id,place,linked=[]) {
		this.Id=Id;
		this.place=place;
		this.linked=linked;
		
		channel_list.push(this);
	}
	
	async link(CH_1){
		let list=[];
		for(i in CH_1.linked){
			if(!list.includes(CH_1.linked[i])){

				list.push(CH_1.linked[i])
			}
		
		}
		
		for(i in this.linked){
			if(!list.includes(this.linked[i])){
				list.push(this.linked[i]);
				
			}
		}
		
		if(!CH_1.linked[0]){
			list.push(CH_1.Id);
		}
		
		
		if(this.linked){
			if(!this.linked[0])
				list.push(this.Id);
		} else
			list.push(this.Id);
		
		console.log("list ");
		console.log(list);
		let ch;
		for(i in list){
			ch= await search_bridge(list[i]);
			ch.linked=list;
			await ch.update_db();
		}
	}
	
	
	async update_db(){
		let ch= {
		Id:this.Id,
		place:this.place,
		linked:this.linked
		};
		dbb.modify_channel(this.Id,{bridge:ch}).then(r=>console.log("AAAAE"));

		
		
	}	
	
}
	


var channel_list=[];



//Browses for channels only in the local buffer
//return object Channel.
//argument: channel Id
function browse_channels(chan_id){
	for (i in channel_list){
		if(channel_list[i].Id==chan_id)
			return channel_list[i];
	}
	
	return null;
}


//Find the channel with the Id, or the channel with the password
//Difference with the previous is that it searches both the local buffer, and the
//mongo database. And it is async
//arguments:
// Param: (string)  / Id or pw.
//Link: (string) "Id" "pw" or "link"
async function  search_bridge(param,link="Id"){
			
	for (i in channel_list){
		if (channel_list[i][link]==param)
			return channel_list[i];
		for(j in channel_list[i][link])
			if (channel_list[i][link][j].Id==param)
				return channel_list[i];
	}
	let ddb= await dbb.find_doc("main.Channels",{id:param});
	
	if(!!ddb){
		if(!!ddb.bridge){
			let CH_1= new channel(ddb.bridge.Id,ddb.bridge.place,ddb.bridge.linked);
			
			return CH_1;
		}
		}

	return null;
}









let message_buffer=[];


function check_replies_buffer(base_string,reply_list){
	if(!reply_list)
		return null;
	if(!base_string)
		return null;
	
	
	let list2=[], list3=[];
	for(i in reply_list){
		list2.push(base_string+reply_list[i]);
	}
	
	let current_m;
	
	for(i in message_buffer){
		for(j in list2){
			if(message_buffer[i].includes(list2[j])){
				list3.push(message_buffer[i])
				current_m=list2[j];
			}
		}
	}
	
	for(i in list3)
		list3[i]=list3[i].join("pppp");
		
	
	list3=list3.join("pppp").split(current_m).join("").split("pppp");
				
	for(i=0;i<list3.length+1;i++){
		current_m=list3.shift();
		if(current_m.length>3)
			list3.push(current_m);
	}
	
	

	
	console.log("list3:");
		console.log(list3);
	return list3;
	
}








//event: type message_event.
//Proprieties:
//body, room_id, event_id, place (string), author (string), reply_ids (string[])
//Methods:
// reply, reply_with_image, has_perm, redact, react
async function _bridge(event){
	if(event.author==config.revolt_id)
		return;
	if(event.author==config.matrix_login)
		return;
	let current_channel= await search_bridge(event.room_id);
	
	if(!current_channel){
		return;
	}
	if(!current_channel.linked){
		return;
	}
	
	let list=[event.place+"--"+event.room_id+"--"+event.event_id];
	
	let place
	let content=event.body;
	let id=await event.get_username(event.author);
	let pfp=await event.get_pfp(event.author);
	let replies=check_replies_buffer(event.place+"--"+event.room_id+"--",event.reply_ids);
	console.log("replies");
	console.log(replies);
	
	let real_reply=[];
	let ii=0,j;
	
	for (ii in current_channel.linked){
		if(current_channel.linked[ii]!=current_channel.Id && !!current_channel.linked[ii]){			
			real_reply=[];
			place=parse_channel(current_channel.linked[ii]);
			for(j in replies){
				if(replies[j].includes(place+"--"+current_channel.linked[ii]))
					real_reply.push(replies[j].split("--")[2]);
			}
			console.log("inside bridge function and ii="+ii);
			console.log(place,content,current_channel.linked[ii],id,pfp,real_reply);
			c= send_masq_message(place,content,current_channel.linked[ii],id,pfp,null,real_reply);
		
			c.then(r=>{	
				list.push(r);
			});
		}
	}
	
	
	message_buffer.push(list);
	
	
	if(event.body.includes("!bridge")){
		console.log(message_buffer);
		let text="Linked Channels:\n";

		a=await search_bridge(event.room_id);
		console.log(a);
		if(!a)
			event.reply("no bridges here");
		if(!a.linked){
			event.reply("liked is null");
			
			return;
		}
		text=text+a.linked.join("\n");
			
		data.event.reply(text)
		

	}
	
	
	
	
}



//Links two channels, used for command
	//data: @Object, the args contain the password.
	//proprieties:
	//	args [ strings ]
	//	place [string]= 'R' or 'M', stand for Revolt or Matrix, might add more
	//	handler @revolt_handler OR @matrix_handler
	//		methods: More than I can write here 
	//	event @m_event ,prefix @string
	//		methods: reply, reply_with_image, has_perm, redact, react
	//		proprieties: body, room_id, event_id, place , author
async function _link(data){
	
	if(data.args[1]){
		let CH_1= await search_bridge(data.event.room_id);

		if(!CH_1)
			CH_1= new channel(data.event.room_id,data.event.place);
		
		
		let CH_2 = await search_bridge(data.args[1],"pw");
		if(!CH_2){
			data.event.reply("Incorrect Password");
			return;
		}
		
		
		
		CH_1.link(CH_2);
		console.log("link command");
		console.log(CH_2);
		console.log(CH_1);
		CH_2.pw=null;
		data.event.reply("Connetion successfull");
	}
	else{
		let CH_1=await search_bridge(data.event.room_id);
		if(!CH_1){
			CH_1= new channel(data.event.room_id,data.event.place);
			channel_list.push(CH_1);
		}
		CH_1.pw=rand_string(20);
		
		data.event.reply("To link the channels, go on the other channel and Do \n"+data.prefix+"link"+" "+CH_1.pw)
	}
}




async function _unlink(data){
	let CH_1= await search_bridge(data.event.room_id);
	let ch_list=[];
	if(!CH_1){
		data.event.reply("This is not bridged");
		return;
	}
	console.log("unlink");
	console.log(CH_1);
	
	let CH_N;
//	let ddb= await dbb.find_doc("main.Channels",{id:param});
	let f=0;
	for(i in CH_1.linked){
		CH_N= search_bridge(CH_1.linked[i]);
		ch_list.push(CH_N);
	}
	
	let array_buffer;
	
	for(i in ch_list){
		ch_list[i]=await ch_list[i];
		//browse channels is a sync function
		for(j in ch_list[i].linked){
			if(ch_list[i].linked[j]==data.event.room_id){
				array_buffer=ch_list[i].linked.splice(j);
				array_buffer.shift();
				ch_list[i].linked.concat(array_buffer);
			}
		}
		if(ch_list[i].Id==data.event.room_id)
			ch_list[i].linked=[];
	}
	
	let bridge;
	for(i in ch_list){
		
		CH_N=ch_list[i]
		CH_N.update_db();
		
	}
	console.log("unlink");
	console.log(ch_list);
	

	data.event.reply("It probably worked");
	
	
}






const bridge = new message_act("bridge",(data) => {return _bridge(data)},{});


const unlink = new Command("unlink",(data) => {return _unlink(data);},{permissions: ["KickMembers"]});




const link = new Command("link",(data) => {return _link(data);},{permissions: ["KickMembers"]});
