
const { Command } = require("../commands");
const {db} = require("../utils/database");
require("../utils/utils");
const configs= require("../config.json");


let fs = require("fs");
const axios = require('axios').default;

var url = "https://comfy.lynx.calitabby.com/";
//proudly hosted by Crispycat
const AUTH={
			"username":configs.APIusername,
			"password":configs.APIpw
};

var dbb=new db();



var global=[];


//Well, this is too simple for a function I think;
function load_workflow(path){
	console.log(path);
	if(!fs.existsSync('./workflow/'+path))
		return -1;
	let str= fs.readFileSync('./workflow/'+path,'utf8'); 
	
	return JSON.parse(str);
}

//DUH why I made this a function...
function save_workflow(namee,json){
	fs.writeFileSync('./workflow/'+namee+'.json', JSON.stringify(json));
}



//ENDPOINT of the comfyUI:
// Is this advertising? Hell yeah:



var comfy_prompt = load_workflow('default.json');

	


//I`m not good with fetch requests. Ill be accepting a push.
//This function sends a GET request, downloads the image.
//Then uploads it right back to the chat
//Then it shifts the queue
//Arguments:
//	data- function data...
//	Id (string) - The prompt ID used to GET the image
function handle_img(Id,data){
	
	

	let filename = data.data[Id].outputs['9'].images[0].filename;

	console.log(filename);
	let link=url+`/view?filename=`+filename+`&subfolder=&type=output`
	if(!filename){
		console.log("ERROR filename undefined");
		return;
		
	}
	
	
	response = axios.get(link, { 
		responseType: 'arraybuffer',
		//'Content-Type':'image/png',
		auth:AUTH
		}).then(async(r)=>{
			console.log("Filename q");
			console.log(filename);
			console.log(Id);
			await fs.writeFile("./outputs/"+Id+"sample.png", r.data, (err) => {
				let start_time;
				console.log(err);
				console.log("error on write file");
				if (err) throw err;		
			})
		
			let m_event ={};
			let q=queue.shift();
			m_event=q.data;
			start_time=q.time;
		
			m_event.event.reply("your image is here, It took \n>"+(Number(Date.now())-start_time)/1000+" seconds to generate");
			m_event.handler.send_image_path(null,"./outputs/"+Id+"sample.png",m_event.event.room_id);
		
		
	
		}).catch(err=>{console.log("error on get_image");
		console.log(err)	});
		
						
						
	setTimeout(()=>{
		if(queue[0])
			query(queue[0])},1000);
	
}




//I`m not good with fetch requests. Ill be accepting a push.
//This function awaits for the image to be ready GET request, downloads the image.
// It sends a GET to the history, to see if the image is ready
// Once its ready it calls handle messages.
//Then uploads it right back to the chat
//Arguments:
//	Id (string) - The prompt ID used to GET the image
function Wait_loop(Id){
	
	let rr= axios.get(url+"/history/"+Id,{  
				"accept": "application/json",
				"Content-Type": "application/json",
						auth:AUTH
		}
	).then(res =>{
		if(!res.data[Id]){
			setTimeout(Wait_loop,10000,Id);
		}
		else{
			setTimeout(handle_img,3000,Id,res);
		}
		})
		
	
}



//Prepares to send the request, and executes the wait loop.
//Arguments: prameters OBJECT - The object contains:
//	data  (OBJECT)- the command data.
//	prompt (STRING) - the text prompt.
//Returns: the request ID, used to fetch the image later.

async function query(prameters) {
	
	if(!prameters)
		return;	
	
	let ddb= await dbb.find_doc("main.Channels",{id:prameters.data.event.room_id});
	if(!ddb)
		ddb= await dbb.find_doc("main.Spaces",{id:prameters.data.event.room_id});
	
	
	//Okay now this gets confusing. this prompt is the workflow+prompt object;
	//in the comfyUI format.
	let prompt;
	
	if(!ddb){
		queue.shift();
		query(queue[0]);
		return;
	}
	else
		prompt=ddb.linx;
	console.log(prompt);
	
	
	prompt.prompt["6"].inputs.text=prameters.prompt;
	prompt.prompt["3"].inputs.seed=getRndInteger(0,10000000);
	
	let Id= await send(prompt);
	
	if(Id==-1){
		setTimeout(query,2000,prameters);
		return;
	}
	
	
	if(typeof(Id)!="string")
		return -1;
	console.log(Id);
	prameters.Id=Id;
	
	Wait_loop(Id);
	return Id;
}


//Makes a request to the COMFYUI to generate a image
//Arguments: workflow (JSON) - the json of all the configurations
//Returns: the request ID, used to fetch the image later.
async function send(workflow) {
		
		url2=url+"/prompt";
		let aa= await axios.post(url2,workflow,
		{  
				"accept": "application/json",
				"Content-Type": "application/json",
						auth:AUTH
		}).catch(err => {console.log("error on send:");
		console.log(err.data)
		url2=-1;
		});
		if(url2==-1)
			return -1;
		return aa.data.prompt_id;
	
}




//Prompt queues, it holds message data, time, and parameters
// The function _gen2 pushes into it
// The function downloadImage POPs.
var queue =[]; //prompt queue

//command to put data in the queue.
// It sends the prompt and data to querry OR pushes to the queue
async function _gen2(data){	
	
	let prompt = data.args.splice(1).join(" ");
	console.log(prompt.includes("girl"));
	if(prompt.includes("girl")){
		prompt=prompt.split("girl").join("woman");
	
	}
	
	if(queue.length>0)
		Gen_message = data.event.reply("Please wait, there are " + queue.length +" generations on the queue");
	else 
		Gen_message = data.event.reply("Generating, please wait");
	
	console.log(Gen_message);
	let prameters ={prompt,data};
	prameters.time=Number(new Date());
	queue.push(prameters);
	
	
	if(queue.length==1){
		query(queue[0]);
	}

}



async function _save(data){	

	let j;
	if(data.args[1]==1){
		j=comfy_prompt;
		
	}
	else{
		j= JSON.parse(data.args.splice(1).join(""));
		
		}	
	let str= data.event.author.replace("@","").replace(":","");



	save_workflow(str,j);
	data.event.reply("Workflow saved as: "+str);

}

async function _gen_settings(data){	
	let args = data.args.splice(1);
	let input={}
	let i;
	
	for(i=0;args[i+1];i++){
		input[args[i]]=args[i+1];
	}
	
	input.model=null;


	
	let ddb= await dbb.find_doc("main.Channels",{id:data.event.room_id});
	if(!ddb)
		ddb= await dbb.find_doc("main.Spaces",{id:data.event.room_id});
	
	let prompt;
	if(!ddb)
		prompt=comfy_prompt
	else
		prompt=ddb.linx;
	
	
	let three_inputs = prompt.prompt['3'].inputs;
	//console.log(three_inputs);
	
	let keys=[];
	
	
	Object.keys(three_inputs).forEach(key => {

		if(input[key]!=undefined){
			three_inputs[key]=input[key];
		} else{	
		}
	})
	
	
	three_inputs['model']=["4", 0];
	//console.log("three inputs:");
	//console.log(three_inputs)
	
	
	
	prompt.prompt['3'].inputs=three_inputs;
	
	
	if(input['negative_prompt']){
		
		prompt.prompt['7'].inputs.text=args.join(" ").split('negative_prompt')[1];
		
	}
	
	await dbb.modify_channel(data.event.room_id,{linx:prompt});
	
	data.event.reply("settings changed");

}


async function _model(data){	
	let model_list=load_workflow("model_list.json");
	let list2= [];
	
	for (i in model_list.list){
		if(model_list.list[i].includes(data.args[1]))
			list2.push(model_list.list[i]);
	} 
	
	if(list2.length>1){
		data.event.reply("I couldnt decide wich one you meant, you have to type the command with one of these:\n```"+list2.join("\n")+"```");
			
	}
	
	
	
	let ddb= await dbb.find_doc("main.Channels",{id:data.event.room_id});
	if(!ddb)
		ddb= await dbb.find_doc("main.Spaces",{id:data.event.room_id});
	
	let prompt;
	if(!ddb)
		prompt=comfy_prompt
	else
		prompt=ddb.linx;
	
	
	
	if (list2.length ==1){
		data.event.reply("model set as: "+list2[0]);
		prompt.prompt['4'].inputs.ckpt_name=list2[0];		
	}
	await dbb.modify_channel(data.event.room_id,{linx:prompt});
	if (list2.length<1)
		data.event.reply("I couldnt find the model, pick one from the list:\n```"+model_list.list.join("\n")+"```");
}


async function _resolution(data){
	let ddb= await dbb.find_doc("main.Channels",{id:data.event.room_id});
	if(!ddb)
		ddb= await dbb.find_doc("main.Spaces",{id:data.event.room_id});
	
	let prompt;
	if(!ddb)
		prompt=comfy_prompt
	else
		prompt=ddb.linx;
	
	let wxh=[data.args[1].split('x')[0], data.args[1].split('x')[1]];
	wxh[0]=Number(wxh[0]);
	wxh[1]=Number(wxh[1]);
	if(wxh[0]>1024 || wxh[1]>1024)
		data.event.reply("Error");
	
	
	prompt.prompt['5'].inputs.width=wxh[0];
	prompt.prompt['5'].inputs.height=wxh[1];
	
	await dbb.modify_channel(data.event.room_id,{linx:prompt});
	
	data.event.reply("resolution changed to ",wxh.join('x'));
}







async function _load(data){	
	let x = load_workflow(data.args[1]+".json");
	if(data.event.author!="01G7W3TE8D134YFACAT31CEH02" && data.event.author!="@plug4090:4d2.org"){
		data.event.reply("Dev_command");
		let ddb= await dbb.find_doc("main.Channels",{id:data.event.room_id});
		if(!ddb)
			return;
		if(!ddb.linx)
			return;
	}

	x.name=data.args[1];
	
	if(!x.prompt){
		data.event.reply("Invalid workflow T_T")
		return;
	}
	console.log(x);
	
	await dbb.modify_channel(data.event.room_id,{linx:	x});
	//comfy_prompt=x;
	data.event.reply("Dev_command, test, it might have worked");
}


async function _print_settings(data){
	
	let ddb= await dbb.find_doc("main.Channels",{id:data.event.room_id});
	if(!ddb.linx){
		data.event.reply("AI not registered on this channel");
		return;
	}
	let prompt=ddb.linx.prompt;
	if(!prompt){
		data.event.reply("AI not registered on this channel");
		return;
	}
	
	
		
	let text="Workflow "+ddb.linx.name+" gen_settings:\n```";
	text=text+"cfg: "+prompt['3'].inputs.cfg+"\n";
	text=text+"steps: "+prompt['3'].inputs.steps+"\n";
	text=text+"sampler_name: "+	prompt['3'].inputs.sampler_name+"\n";
	text=text+"denoise: "+prompt['3'].inputs.denoise+"\n";
	text=text+"model: "+prompt['4'].inputs.ckpt_name+"\n";
	text=text+"Resolution: "+prompt['5'].inputs.height+'x'+prompt['5'].inputs.width+"\n";
	text=text+"negative_prompt: "+prompt['7'].inputs.text+"\n";
	data.event.reply(text+"```");
	
}



//Description: Changes the model from the generation on the channel
//Or shows the list of models that match the criteria
//Arguments: Model name;
const mode = new Command("model",(data) => {return _model(data);},null);


//Description: Generates image with the prompt
const gen2 = new Command("g2",(data) => {return _gen2(data);},null);



//WIP command, saves the workflow for later usage
//arguments: 1 or json
//case 1 - Saves the current workflow on the channel
//case json- saves a json string as a worflow. 
const save = new Command("save",(data) => {return _save(data);},null);


//Loads a workflow previously saved
const load = new Command("load",(data) => {return _load(data);},null);

//Description:Changes the resolution:
//Arguments int + 'x' + int
// resolution 512x512
const res = new Command("resolution",(data) => {return _resolution(data);},null);


//Description:Changes other gen settings:
const gen_settings = new Command("gen_settings",(data) => {return _gen_settings(data);},null);


//Description: Displays the generation settings of that channel

const settings_show = new Command("print_settings",(data) => {return _print_settings(data);},null);







