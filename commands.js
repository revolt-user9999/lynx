

var list=[];

class Command {
	constructor(Name,fun,extras) {
		this.Name=Name;
		this.fun=fun;
		this.extras=extras; //extras type list. Or json, with permission list, or role list.
		list.push(this);
	}
	run(args,handler,event,prefix){
		this.fun({args,handler,event,prefix});
	}

}

function handle_commands(event,prefix, handler){
	let event_body= event.body;
	if(!event_body.startsWith(prefix))
		return;
	let args=event_body.split(" ");
	let command =args[0].split(prefix)[1];
	let c=[];
	let i=0;
	for (i in list){
		if(list[i].Name==command)
			 c.push(list[i]);
	
	}

	if(!c[0])
		return;

	if(!c[0].extras)
		c[0].run(args,handler,event,prefix);
	else{
		
		i= event.has_perm(c[0].extras.permissions);
		if(i>0)
			c[0].run(args,handler,event,prefix);
		else{
			let no_message=[`Hey, you! Yes, you! The one who thinks they can just waltz in and use moderation commands. Well, let me tell you something, you don't have the keys to the kingdom. You're not a member of the secret club of moderators, so you can't just go around using commands like you own the place!'`, `Remember, every command has its place and time. Some commands are for the big shots, the ones with the power to control the server. But not everyone is a big shot. Some of us are just regular users, trying to get by. We don't have the power to use all commands, but that doesn't mean we can't enjoy the server. We just need to find our place, and that's not in the admin's office.'`];
			
			handler.reply(event,no_message.join("\n"));
			
			
			}
			
		}
		
		
		
		
}
	
	
module.exports = { handle_commands,Command };