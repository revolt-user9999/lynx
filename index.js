
const configs= require("./config.json");



//Revolt functions ==========
const Revolt = require("revolt.js");
const { Revolthandler } = require("./handlers/revolt_handle");




const rev_client = new Revolt.Client();
rev_client.loginBot(configs.revolt_bot_token)

const rev_handler = new Revolthandler(rev_client);




//matrix functions ========
const { MatrixClient,
    SimpleFsStorageProvider,
    AutojoinRoomsMixin,RustSdkAppserviceCryptoStorageProvider} = require("matrix-bot-sdk");
const { Matrix_handler } = require("./handlers/matrix_handle");
const storage = new SimpleFsStorageProvider("hello-bot.json");// idk what the storage is for. but its there for a reason I think.




const cryptoProvider = new RustSdkAppserviceCryptoStorageProvider("./encryption",);	

const mat_client = new MatrixClient(configs.matrix_domain, configs.matrix_token, storage,cryptoProvider);



AutojoinRoomsMixin.setupOnClient(mat_client); //Remove this line to NOT autojoin rooms


const mat_handler = new Matrix_handler(mat_client);

require("./modules/admin.js");
//require("./modules/ai.js");
require("./modules/ping.js");
require("./modules/linx.js");
require("./bridge/bridge.js");
require("./modules/dev_tools.js");


const { handle_messages,get_handler } = require("./message_events");




let prefix="!";




//Revolt message event
rev_client.on("messageCreate", (message) => { //Revolt message event

	
	handle_messages(message, message.channel.id,"R" ,rev_handler);
	
	
});




//matrix message event
mat_client.on("room.message", (roomId, event) => {
	
	if (!event['content']?.['msgtype']) return;
	if (!event['sender']) return;
	
	handle_messages(event, roomId, "M",mat_handler);
});

setTimeout(get_handler, 20000, [mat_handler,rev_handler]);

	mat_client.start().then(() => {console.log("Matrix Bot started!");


	get_handler([mat_handler,rev_handler]);

});



process.on("uncaughtException", (err) => {
    console.error(`Uncaught error:\n${err.stack}`);
	console.log(err);
});
